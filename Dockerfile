FROM openjdk:17-alpine

ADD target/anime-observer-core-1.0.0-SNAPSHOT.jar /anime-observer-core-1.0.0-SNAPSHOT.jar
ENTRYPOINT ["java", "-XX:HeapDumpPath=\"heapDump.hprof\"", "-XX:+CrashOnOutOfMemoryError", "-jar", "anime-observer-core-1.0.0-SNAPSHOT.jar"]