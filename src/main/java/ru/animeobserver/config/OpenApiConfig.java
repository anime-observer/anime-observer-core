package ru.animeobserver.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI mortgageOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("anime-observer-core API")
                        .description("Place here description")
                        .version("v0.0.1"));
    }
}