package ru.animeobserver.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@UtilityClass
public class TestFileUtils {

    @SneakyThrows
    public static <T> T loadFromJson(String name, Class<? extends T> type, @NonNull ObjectMapper objectMapper) {
        File file = getFile(name);
        return objectMapper.readValue(file, type);
    }

    @SneakyThrows
    public static <T> T loadFromJson(String name, TypeReference<T> type, @NonNull ObjectMapper objectMapper) {
        File file = getFile(name);
        return objectMapper.readValue(file, type);
    }

    @SneakyThrows
    public static String getStringContent(String filePath) {
        return FileUtils.readFileToString(getFile(filePath), StandardCharsets.UTF_8.name());
    }

    public static File getFile(String name) {
        return new File(Objects.requireNonNull(TestFileUtils.class.getClassLoader().getResource(name)).getFile());
    }
}
